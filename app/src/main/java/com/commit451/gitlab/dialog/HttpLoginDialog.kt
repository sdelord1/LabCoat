package com.commit451.gitlab.dialog

import android.content.Context
import androidx.appcompat.app.AppCompatDialog
import com.commit451.gitlab.R
import com.commit451.gitlab.databinding.DialogHttpLoginBinding

class HttpLoginDialog(context: Context, realm: String, loginListener: LoginListener) :
    AppCompatDialog(context) {

    private var binding: DialogHttpLoginBinding = DialogHttpLoginBinding.inflate(layoutInflater)

    init {
        setContentView(binding.root)

        binding.textMessage.text =
            String.format(context.resources.getString(R.string.realm_message), realm)
        binding.buttonOk.setOnClickListener {
            loginListener.onLogin(
                binding.textUsername.text.toString(),
                binding.textPassword.text.toString()
            )
            dismiss()
        }
        binding.buttonCancel.setOnClickListener {
            loginListener.onCancel()
            dismiss()
        }
        setTitle(R.string.login_activity)
    }

    interface LoginListener {
        fun onLogin(username: String, password: String)
        fun onCancel()
    }
}
