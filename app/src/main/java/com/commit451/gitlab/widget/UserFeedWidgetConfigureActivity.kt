package com.commit451.gitlab.widget

import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.commit451.gitlab.R
import com.commit451.gitlab.activity.BaseActivity
import com.commit451.gitlab.data.Prefs
import com.commit451.gitlab.databinding.ActivityFeedWidgetConfigureBinding
import com.commit451.gitlab.model.Account
import timber.log.Timber

/**
 * The configuration screen for the ExampleAppWidgetProvider widget sample.
 */
class UserFeedWidgetConfigureActivity : BaseActivity() {

    private lateinit var binding: ActivityFeedWidgetConfigureBinding
    private lateinit var adapterAccounts: AccountsAdapter

    private var appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID

    public override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)
        setResult(Activity.RESULT_CANCELED)
        binding = ActivityFeedWidgetConfigureBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Find the widget id from the intent.
        val intent = intent
        val extras = intent.extras
        if (extras != null) {
            appWidgetId = extras.getInt(
                AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID
            )
        }
        // If they gave us an intent without the widget id, just bail.
        if (appWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish()
        }

        binding.toolbar.setTitle(R.string.widget_choose_account)

        adapterAccounts = AccountsAdapter(object : AccountsAdapter.Listener {
            override fun onAccountClicked(account: Account) {
                saveWidgetConfig(account)
            }

        })
        binding.list.layoutManager = LinearLayoutManager(this)
        binding.list.adapter = adapterAccounts

        loadAccounts()
    }

    private fun loadAccounts() {
        val accounts = Prefs.getAccounts()
        Timber.d("Got %s accounts", accounts.size)
        accounts.sort()
        accounts.reverse()
        if (accounts.isEmpty()) {
            binding.textMessage.visibility = View.VISIBLE
        } else {
            binding.textMessage.visibility = View.GONE
            adapterAccounts.clearAndFill(accounts)
        }
    }

    private fun saveWidgetConfig(account: Account) {
        UserFeedWidgetPrefs.setAccount(this@UserFeedWidgetConfigureActivity, appWidgetId, account)
        val appWidgetManager = AppWidgetManager.getInstance(this@UserFeedWidgetConfigureActivity)

        // Make sure we pass back the original appWidgetId
        val data = Intent()
        val extras = Bundle()
        extras.putInt(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
        data.putExtras(extras)
        setResult(Activity.RESULT_OK, data)
        appWidgetManager.updateAppWidgetOptions(appWidgetId, extras)
        finish()
        //Manually have to trigger on update here, it seems
        WidgetUtil.triggerWidgetUpdate(this, UserFeedWidgetProvider::class.java, appWidgetId)
    }

}
