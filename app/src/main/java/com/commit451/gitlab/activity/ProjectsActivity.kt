package com.commit451.gitlab.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import com.commit451.gitlab.App
import com.commit451.gitlab.R
import com.commit451.gitlab.adapter.ProjectsPagerAdapter
import com.commit451.gitlab.databinding.ActivityProjectsBinding
import com.commit451.gitlab.event.CloseDrawerEvent
import com.commit451.gitlab.navigation.Navigator
import org.greenrobot.eventbus.Subscribe

/**
 * Shows the projects
 */
class ProjectsActivity : BaseActivity() {

    companion object {

        fun newIntent(context: Context): Intent {
            return Intent(context, ProjectsActivity::class.java)
        }
    }

    private lateinit var binding: ActivityProjectsBinding

    private val onMenuItemClickListener = Toolbar.OnMenuItemClickListener { item ->
        when (item.itemId) {
            R.id.action_search -> {
                Navigator.navigateToSearch(this@ProjectsActivity)
                return@OnMenuItemClickListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProjectsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        App.bus().register(this)

        binding.toolbar.setTitle(R.string.projects)
        binding.toolbar.setNavigationIcon(R.drawable.ic_menu_24dp)
        binding.toolbar.setNavigationOnClickListener { binding.drawerLayout.openDrawer(GravityCompat.START) }
        binding.toolbar.inflateMenu(R.menu.search)
        binding.toolbar.setOnMenuItemClickListener(onMenuItemClickListener)
        binding.viewPager.adapter = ProjectsPagerAdapter(this, supportFragmentManager)
        binding.tabLayout.setupWithViewPager(binding.viewPager)
    }

    override fun onDestroy() {
        super.onDestroy()
        App.bus().unregister(this)
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    @Suppress("UNUSED_PARAMETER")
    @Subscribe
    fun onEvent(event: CloseDrawerEvent) {
        binding.drawerLayout.closeDrawers()
    }
}
