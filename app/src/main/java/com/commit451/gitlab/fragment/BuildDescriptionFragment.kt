package com.commit451.gitlab.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.commit451.addendum.design.snackbar
import com.commit451.gitlab.App
import com.commit451.gitlab.R
import com.commit451.gitlab.databinding.FragmentBuildDescriptionBinding
import com.commit451.gitlab.event.BuildChangedEvent
import com.commit451.gitlab.extension.with
import com.commit451.gitlab.model.api.*
import com.commit451.gitlab.util.DateUtil
import com.google.android.material.snackbar.Snackbar
import org.greenrobot.eventbus.Subscribe
import timber.log.Timber
import java.util.*

/**
 * Shows the details of a build
 */
class BuildDescriptionFragment : BaseFragment() {

    companion object {

        private const val KEY_PROJECT = "project"
        private const val KEY_BUILD = "build"

        fun newInstance(project: Project, build: Build): BuildDescriptionFragment {
            val fragment = BuildDescriptionFragment()
            val args = Bundle()
            args.putParcelable(KEY_PROJECT, project)
            args.putParcelable(KEY_BUILD, build)
            fragment.arguments = args
            return fragment
        }
    }

    private var binding: FragmentBuildDescriptionBinding? = null
    private lateinit var project: Project
    private lateinit var build: Build

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        project = arguments?.getParcelable(KEY_PROJECT)!!
        build = arguments?.getParcelable(KEY_BUILD)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBuildDescriptionBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.swipeRefreshLayout?.setOnRefreshListener { load() }
        bindBuild(build)
        App.bus().register(this)
    }

    override fun onDestroyView() {
        App.bus().unregister(this)
        super.onDestroyView()
        binding = null
    }

    fun load() {
        App.get().gitLab.getBuild(project.id, build.id)
            .with(this)
            .subscribe({
                binding?.swipeRefreshLayout?.isRefreshing = false
                build = it
                bindBuild(build)
                App.bus().post(BuildChangedEvent(it))
            }, {
                Timber.e(it)
                binding?.root?.snackbar(R.string.unable_to_load_build, Snackbar.LENGTH_LONG)
            })
    }

    private fun bindBuild(build: Build) {
        var finishedTime: Date? = build.finishedAt
        if (finishedTime == null) {
            finishedTime = Date()
        }
        var startedTime: Date? = build.startedAt
        if (startedTime == null) {
            startedTime = Date()
        }
        val name = String.format(getString(R.string.build_name), build.name)
        binding?.textName?.text = name
        val pipelineText = String.format(getString(R.string.build_pipeline), build.pipeline)
        binding?.textPipeline?.text = pipelineText
        val stage = String.format(getString(R.string.build_stage), build.stage)
        binding?.textStage?.text = stage
        val status = String.format(getString(R.string.build_status), build.status)
        binding?.textStatus?.text = status
        val timeTaken = DateUtil.getTimeTaken(startedTime, finishedTime)
        val duration = String.format(getString(R.string.build_duration), timeTaken)
        binding?.textDuration?.text = duration
        val created = String.format(
            getString(R.string.build_created),
            DateUtil.getRelativeTimeSpanString(baseActivity, build.createdAt)
        )
        binding?.textCreated?.text = created
        val ref = String.format(getString(R.string.build_ref), build.ref)
        binding?.textRef?.text = ref
        val finishedAt = build.finishedAt
        if (finishedAt != null) {
            val finished = String.format(
                getString(R.string.build_finished),
                DateUtil.getRelativeTimeSpanString(baseActivity, finishedAt)
            )
            binding?.textFinished?.text = finished
            binding?.textFinished?.visibility = View.VISIBLE
        } else {
            binding?.textFinished?.visibility = View.GONE
        }
        val runner = build.runner
        if (runner != null) {
            bindRunner(runner)
        }
        val pipeline = build.pipeline
        if (pipeline != null) {
            bindPipeline(pipeline)
        }
        val commit = build.commit
        if (commit != null) {
            bindCommit(commit)
        }
    }

    private fun bindRunner(runner: Runner) {
        val runnerNum = String.format(getString(R.string.runner_number), runner.id.toString())
        binding?.textRunner?.text = runnerNum
    }

    private fun bindPipeline(pipeline: Pipeline) {
        val pipelineNum = String.format(getString(R.string.build_pipeline), pipeline.id.toString())
        binding?.textPipeline?.text = pipelineNum
    }

    private fun bindCommit(commit: RepositoryCommit) {
        val authorText = String.format(getString(R.string.build_commit_author), commit.authorName)
        binding?.textAuthor?.text = authorText
        val messageText = String.format(getString(R.string.build_commit_message), commit.message)
        binding?.textMessage?.text = messageText
    }

    @Suppress("unused")
    @Subscribe
    fun onBuildChangedEvent(event: BuildChangedEvent) {
        if (build.id == event.build.id) {
            build = event.build
            bindBuild(build)
        }
    }
}
