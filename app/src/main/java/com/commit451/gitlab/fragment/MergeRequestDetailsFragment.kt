package com.commit451.gitlab.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coil.load
import coil.transform.CircleCropTransformation
import com.commit451.gitlab.App
import com.commit451.gitlab.R
import com.commit451.gitlab.adapter.IssueLabelsAdapter
import com.commit451.gitlab.databinding.FragmentIssueDetailsBinding
import com.commit451.gitlab.event.MergeRequestChangedEvent
import com.commit451.gitlab.extension.setMarkdownText
import com.commit451.gitlab.model.api.MergeRequest
import com.commit451.gitlab.model.api.Project
import com.commit451.gitlab.util.DateUtil
import com.commit451.gitlab.util.ImageUtil
import com.commit451.gitlab.util.InternalLinkMovementMethod
import com.commit451.gitlab.viewHolder.IssueLabelViewHolder
import org.greenrobot.eventbus.Subscribe

/**
 * Shows the discussion of an issue
 */
class MergeRequestDetailsFragment : BaseFragment() {

    companion object {

        private const val KEY_PROJECT = "project"
        private const val KEY_MERGE_REQUEST = "merge_request"

        fun newInstance(project: Project, mergeRequest: MergeRequest): MergeRequestDetailsFragment {
            val fragment = MergeRequestDetailsFragment()
            val args = Bundle()
            args.putParcelable(KEY_PROJECT, project)
            args.putParcelable(KEY_MERGE_REQUEST, mergeRequest)
            fragment.arguments = args
            return fragment
        }
    }

    private var binding: FragmentIssueDetailsBinding? = null
    private lateinit var adapterLabels: IssueLabelsAdapter

    private lateinit var project: Project
    private lateinit var mergeRequest: MergeRequest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        project = arguments?.getParcelable(KEY_PROJECT)!!
        mergeRequest = arguments?.getParcelable(KEY_MERGE_REQUEST)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentIssueDetailsBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapterLabels = IssueLabelsAdapter(object : IssueLabelsAdapter.Listener {
            override fun onLabelClicked(label: String, viewHolder: IssueLabelViewHolder) {

            }
        })
        binding?.listLabels?.adapter = adapterLabels

        bind(mergeRequest, project)

        App.bus().register(this)
    }

    override fun onDestroyView() {
        App.bus().unregister(this)
        super.onDestroyView()
        binding = null
    }

    fun bind(mergeRequest: MergeRequest, project: Project) {

        if (mergeRequest.description.isNullOrEmpty()) {
            binding?.textDescription?.visibility = View.GONE
        } else {
            binding?.textDescription?.visibility = View.VISIBLE
            binding?.textDescription?.setMarkdownText(mergeRequest.description!!, project)
            binding?.textDescription?.movementMethod =
                InternalLinkMovementMethod(App.get().getAccount().serverUrl!!)
        }

        binding?.imageAuthor?.load(
            ImageUtil.getAvatarUrl(
                mergeRequest.author,
                resources.getDimensionPixelSize(R.dimen.image_size)
            )
        ) {
            transformations(CircleCropTransformation())
        }

        var author = ""
        if (mergeRequest.author != null) {
            author = mergeRequest.author!!.name + " "
        }
        author += resources.getString(R.string.created_issue)
        if (mergeRequest.createdAt != null) {
            author = author + " " + DateUtil.getRelativeTimeSpanString(
                baseActivity,
                mergeRequest.createdAt
            )
        }
        binding?.textAuthor?.text = author
        if (mergeRequest.milestone != null) {
            binding?.rootMilestone?.visibility = View.VISIBLE
            binding?.textMilestone?.text = mergeRequest.milestone!!.title
        } else {
            binding?.rootMilestone?.visibility = View.GONE
        }
        adapterLabels.setLabels(mergeRequest.labels)
    }

    @Subscribe
    fun onEvent(event: MergeRequestChangedEvent) {
        if (mergeRequest.iid == event.mergeRequest.iid) {
            mergeRequest = event.mergeRequest
            bind(mergeRequest, project)
        }
    }
}
